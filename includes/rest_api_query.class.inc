<?php
/**
 * @file
 * Defines the rest_api_query class; a base for other modules to extend.
 */

/**
 * Base class for building and executing RESTful API queries.
 */
abstract class rest_api_query {
  // Prevents execution if initialization failed.
  protected $initialized = FALSE;

  /**
   * Components to build the HTTP request.
   */

  // Access point.
  protected $scheme = 'http';
  protected $host;
  protected $port;
  protected $path = '';

  // @todo Postponed: Support other formats.
  protected $format = 'json';

  // Authentication credentials.
  protected $api_key;
  protected $username;
  protected $password;

  // The object type, object ID and METHOD for the query.
  public $method = 'GET';
  public $resource;
  public $identifier;

  // Other public parameters.
  public $parameters = array();
  public $headers = array(
    'Content-type' => 'application/json',
  );

  // The body of the message to send.  It will be encoded with drupal_to_js().
  public $data;


  /**
   * Properties for configuration, internal use and read-only response data.
   */

  // The fully assembled URI for the access point.  URI parts may be set through
  // this property.  It has a magic getter and setter so can be accessed
  // externally, despite it's "private" visibility.
  private $access_point;

  // The fully assembled URL, including the resource and identifier.
  // Internal use only, though has a magic getter for external read access.
  private $url;

  // The response object from drupal_http_request().
  protected $response;

  // Error code and error message, if any.
  protected $code;
  protected $error = FALSE;

  // The decoded response.
  protected $result;

  // When do caches expire?
  // Defaults to one hour; 60 * 60.
  protected $cache_validity = 3600;

  /**
   * Magic method to allow all private and protected properties to be read.
   */
  function __get($name) {
    // Use the getter function if there is one.
    $func = "get_$name";
    if (method_exists($this, $func)) {
      return $this->$func();
    }

    // Allow read-only access to all protected properties.
    return $this->$name;
  }

  /**
   * Magic method to allow certain private properties to be set.
   *
   * To make a private property setttable through this magic method, implement a
   * "set_NAME" method where NAME is the private property to set.
   */
  function __set($name, $value) {
    $func = "set_$name";
    if (method_exists($this, $func)) {
      $this->$func($value);
    }
  }

  public function set_access_point($url) {
    $url = parse_url($url);
    $properties = array(
                'scheme',
                'host',
                'port',
                'path',
      'user' => 'username',
      'pass' => 'password',
    );
    foreach ($properties as $key => $property) {
      if (is_numeric($key)) {
        $key = $property;
      }
      $this->$property = $url[$key];
    }
  }

  public function get_access_point() {
    if (!$this->initialized || empty($this->scheme) || empty($this->host)) {
      return FALSE;
    }

    // Assemble the bits for the URL.
    // Most REST APIs allow authentication with the API key as the BasicAuth "user".
    if (!empty($this->api_key)) {
      $this->username = $this->api_key;
    }

    $authentication = !empty($this->username) ? $this->username : '';
    if (!empty($this->password)) {
      $authentication .= ':' . $this->password;
    }
    if (!empty($authentication)) {
      $authentication .= '@';
    }

    $port = !empty($this->port) ? ':' . $this->port : '';
    $path = !empty($this->path) ? '/' . $this->path : '';

    return $this->scheme . '://' . $authentication . $this->host . $port . $path;
  }

  public function get_url() {
    $this->access_point = $this->get_access_point();
    if (!$this->initialized || !$this->access_point) {
      // Do not execute if initialization failed.
      return FALSE;
    }

    $url = array(
      $this->access_point,
      $this->resource,
    );
    if ($this->identifier) {
      $url[] = $this->identifier;
    }
    $url = implode('/', $url) . '.' . $this->format;

    // Have Drupal check the URL and handle the HTTP request.
    return url($url, array('query' => $this->parameters, 'external' => TRUE));
  }

  /**
   * Set or overwrite an individual query string parameter.
   *
   * @param $name String
   *    The key of the parameter to set.
   * @param $value String
   *    The value to set the parameter to.
   */
  public function set_parameter($name, $value) {
    $this->parameters[$name] = $value;
  }

  /**
   * Set or overwrite query string parameters.
   *
   * @param $parameters Array(String => String)
   *    The key of the parameter to set.
   * @param $value String
   *    The value to set the parameter to.
   */
  public function set_parameters(Array $parameters) {
    // @todo Use array_merge().
    foreach ($parameters as $name => $value) {
      $this->parameters[$name] = $value;
    }
  }

  public function cacheable() {
    // Cache does not work if the cache tables are not present.  Allow the user
    // to disable caching if using this in database-less environment, such as
    // drush commands or Simpletest DrupalUnitTestCase.
    // @see RestApiQueryUnitTestCase.class.inc
    return empty($GLOBALS['rest_api_query_disable_cache']) && $this->method == 'GET';
  }

  /**
   * Executes the query by requesting data via HTTP from the redmine server.
   *
   * @return rest_api_query
   *    $this, So that the return value can be chained.  Check the "error" and
   *    "code" property for status.  For example;
   *      @code
   *      if (empty($query->execute()->error)) {
   *        // Success!
   *      }
   *      @endcode
   */
  public function execute() {
    $this->reset();

    if ($this->cacheable()) {
      // Generate a hash that will serve as a unique identifier for this query.
      $cache = cache_get($this->url, 'cache_rest_api_query');

      // Have we cached this query?  Is it less than one hour old?
      if (isset($cache->data) && $cache->created > (time() - $this->cache_validity)) {
        $this->response = $cache->data;
        tve($this->response, 'response, cache HIT');

        // Include the cache info in the response, but do not duplicate the data.
        unset($cache->data);
        $this->cache = $cache;
      }
    }

    if (empty($this->response)) {
      $this->url = $this->get_url();
      $data = empty($this->data) ? NULL : drupal_to_js($this->data);
      // @todo Postponed: Support rest_client_request() in rest_client.module?
      $this->response = drupal_http_request($this->url, $this->headers, $this->method, $data);
      // tve($this->response, 'Response, cache MISS');
    }


    // Set this object's status code and error message, if any.
    $this->code = $this->response->code;
    if (isset($this->response->error)) {
      $this->error = $this->response->error;
    }

    // If there is any data in the response, attempt to decode it.
    if (!empty($this->response->data)) {
      $this->result = json_decode($this->response->data);
    }

    // Return the error message if any.
    return $this;
  }

  public function reset() {
    $this->response = NULL;
    $this->code = NULL;
    $this->error = FALSE;
    $this->result = NULL;
  }
}
