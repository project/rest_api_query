<?php
/**
 * @file
 * Hook definitions and example implementations.
 */

/**
 * Defines REST API Schemas.
 *
 * @return Array(String => stdClass)
 *    The index is the machine name of the API schema.  The module name is
 *    usually suitable, unless the module exposes multiple schemas.  The value is
 *    an object with the following properties;
 *      - "types" Array(String => String):
 *          The index is *singular* name of the type.  The value is the *plural*
 *          name of the type.  "Type" is often referred to as "resource".
 *      - "class" optional String:
 *          The name of this module's class which inherits from the
 *          rest_api_query class.
 *          Defaults to "[schema]_rest_api_query" where "[schema]" is the machine
 *          name of the schema.
 *      - "file" optional String:
 *          The file path name where the class is defined, relative to the
 *          module's root directory.  E.g. "includes/redmine_query.class.api".
 *          Defaults to "[class].class.inc" where "[class]" is the name of the
 *          class.
 */
function hook_rest_api_schemas() {
  $schemas = array();

  $schemas['redmine'] = new stdClass();
  $schemas['redmine']->class = 'redmine_query';
  $schemas['redmine']->types = array(
    'issue' => 'issues',
    'project' => 'projects',
    'time_entry' => 'time_entries',
    'user' => 'users',
  );

  return $schemas;
}
