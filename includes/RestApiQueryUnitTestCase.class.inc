<?php
/**
 * @file
 * Expose classes to facilitate running unit tests with SimpleTest module.
 */

/**
 * Extends Simpletest module's DrupalUnitTestCase for modules that implement RAQ.
 *
 * Unit tests are fast and cheap to spin up and tear down, but do not have
 * database tables, and thus do not support caching.  DrupalWebTestCase tests do
 * have tables but are far more expensive to run due to the large amounts of data
 * duplication required.  RestApiQueryUnitTestCase allows modules which consume
 * REST APIs to run functional unit tests, even without the cache tables by
 * temporarily disabling REST API Query module's caching features.
 */
abstract class RestApiQueryUnitTestCase extends DrupalUnitTestCase {
  /**
   * Disable REST API Query's caching features.
   */
  function setUp() {
    $global['rest_api_query_disable_cache'] = TRUE;
  }

  /**
   * Re-enable REST API Query's caching features.
   */
  function tearDown() {
    unset($global['rest_api_query_disable_cache']);
  }
}
